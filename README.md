Git Training Aachen December 2016
---------------------------------

This will be a small log for what we did:

 - we started with git init
 - use git config to view or change configuration
 - use aliases to easen work
 - we used a branch to do some experiments and called it testing
 - fast forward merges are great
 - most likely we will not be able to fast forward now
 - git reset is a poweful tool
 - please remember to specify --soft, --mixed or --hard
 - you can also rewrite history and elder commits
 - we then added a remote repository
 - you can use pull to read from remote
 - better: use fetch + merge
 - don't commit to master! Just merge!
 - try to avoid merge commits when pushing
 - stick with feature branches
 - remember that there is tool called rebase

 - beware: must be used with caution
